
import fs from "fs"
import path from "path"

import less from "less"
import { minify } from 'csso'

import nunjucks from "nunjucks"



export default class LessCompiler {

  constructor(wctx_path, config, web_dir_path, context) {
    this.context = context || {};
    config = config || {};
    

    this.minify = config.minify;

    this.wctx_path = wctx_path;

    this.less_path = path.resolve(wctx_path, config.src);
    this.dst_path = path.resolve(wctx_path, config.dst);

    this.web_dir_path = web_dir_path;

    this.less_imports = [];

    this.watch = {};
  }

  parse_imports(less_src, path_prefix, watch) {
    const _this = this;

    less_src = less_src.replace(/@import (?:'(\/.*)'|"(\/.*)"|`(\/.*)`)/gm, `@import '${this.web_dir_path}$1$2$3'`)
    less_src = less_src.replace(/@import (?:'([^\/].*)'|"([^\/].*)"|`([^\/].*)`)/gm, `@import '${path_prefix || this.wctx_path}/$1$2$3'`)


    const import_regexp = /@import (?:'(\/.*)'[;]?|"(\/.*)"[;]?|`(\/.*)`[;]?)/gm

    let matches = null;
    while ((matches = import_regexp.exec(less_src)) !== null) {
      if (this.watch.less && !this.less_imports.includes(matches[1])) {
        this.less_imports.push(matches[1]);
        fs.watchFile(matches[1], { interval: 1000 }, function(curr, prev) {
          _this.compile();
        });
      }

      let import_src = nunjucks.renderString(fs.readFileSync(matches[1], 'utf8'), this.context);
      let npfix = matches[1].split("/");
      npfix.pop();
      npfix = npfix.join("/");
      import_src = this.parse_imports(import_src, npfix);

      let rpl_regex = matches[0];
      rpl_regex = rpl_regex.replace(/\//g, "\\/");
      rpl_regex = rpl_regex.replace(/\./g, "\\.");
      less_src = less_src.replace(new RegExp(rpl_regex, "gm"), import_src);

    }


    return less_src;
  }

  async compile() {
    try {
      let _this = this;
      let file = this.less_path;
      if (fs.existsSync(file)) {
        let less_src = nunjucks.renderString(fs.readFileSync(file, 'utf8'), this.context);
        less_src = this.parse_imports(less_src);

/*
        const ff_regex = /@font-face\s*{[^}]+}/gm;
        const ff_url_regex = /@font-face\s*{[^{}]+src:\s*url\(['"`]([^'"\`]+)['"`][^{}]+}/m;

        const ff_matches = less_src.match(ff_regex);

        if (ff_matches) {
          for (let ffm of ff_matches) {
            const ff_url = ffm.match(ff_url_regex)[1];
            const full_font_path = path.resolve(this.web_dir_path, ff_url.slice(1));
            console.log(full_font_path);
            const font_file_data = fs.readFileSync(full_font_path);
            const base64_url = `data:font/ttf;base64,${font_file_data.toString("base64")}`;
            ffm = ffm.replace(/(@font-face\s*{[^}]+src:\s*url\(['"`])([^'"\`]+)(['"`][^{}]+})/m , `$1${base64_url}$3`)
            less_src = less_src.replace(ff_regex, ffm);
          }
        }*/


        await new Promise((fulfil) => {
          less.render(less_src, {
            relativeUrls: true,
            paths: [this.dirname]
          }, function(err, output) {
            let report_time = new Date();
            report_time = report_time.toDateString() + " " + report_time.toTimeString().slice(0, report_time.toTimeString().indexOf("GMT")-1);
            if (err) {
              console.error("");
              console.error("\x1b[34m"+report_time+" \x1b[31mLESS COMPILATION ERROR:")
              console.error("\x1b[33m"+err.message+" --- \x1b[32mline: "+err.line+"\x1b[0m");
              console.error("\x1b[36m");
              for (let e = 0; e < err.extract.length; e++) {
                if (err.extract[e]) console.error(err.extract[e]);
              }
              console.error("\x1b[0m");
              fulfil(false);
            } else {
              let output_css = output.css;
              if (_this.minify) output_css = minify(output_css).css;
              fs.writeFileSync(_this.dst_path, output_css);
              console.log("\x1b[34m"+report_time, "\x1b[1m\x1b[32mLESS\x1b[0m ----->\x1b[36m", _this.dst_path+"\x1b[0m");
              fulfil(true);
            }
          });
        });
      }
    } catch (e) {
      console.error(e.stack);
    }
  }

  watch() {
    let _this = this;

    this.watch.less = fs.watchFile(this.less_path, { interval: 1000 }, function(curr, prev) {
      _this.compile();
    });
    this.compile();
  }

  stop_watch() {
    if (this.watch.less) fs.unwatchFile(this.less_path);
    for (const impo of this.less_imports) {
      fs.unwatchFile(impo);
    }
    this.less_imports = [];
    this.watch.less = false;
  }

}
