


import path from "path";
import fs from "fs";

import YAML from "yaml";

import LessCompiler from "./compilers/less/less.mjs"
import MJSCompiler from "./compilers/mjs/webpack.mjs"


import ObjectUtil from 'zunzun/flyutil/object.mjs'
import FSUtil from 'zunzun/flyutil/fs.mjs'

const Compilers = {
  less: LessCompiler,
  mjs: MJSCompiler
};


const CWD = process.cwd();

export default class FlyPack {
  constructor(config, zuncfg) {
    this.config = config;
    this.zuncfg = zuncfg;
  }

  async bundle_context(web_dir_path, ctx_path, app_dir) {
    try {
      const _this = this;

      const flycfg_path = path.resolve(ctx_path, this.config.config_name);
      let flycfg = undefined;
      if (fs.existsSync(flycfg_path)) {
        try {
          flycfg = YAML.parse(fs.readFileSync(flycfg_path, 'utf8'));
        } catch (e) {
          console.error(e.stack);
        }
      }
      if (!flycfg) flycfg = {};

      for (let ctag in Compilers) {
        if (!flycfg[ctag]) flycfg[ctag] = this.config[ctag];
        if (!flycfg[ctag].src) flycfg[ctag].src = this.config[ctag].src;
        if (!flycfg[ctag].dst) flycfg[ctag].dst = this.config[ctag].dst;
        if (fs.existsSync(path.resolve(ctx_path, flycfg[ctag].src))) {
          flycfg[ctag].cwd = app_dir;
          const compiler = new Compilers[ctag](ctx_path, flycfg[ctag], web_dir_path);
          if (process.argv[3] == "watch") {
            compiler.watch();
          } else {
            await compiler.compile();
          }
        }

        if (flycfg[ctag].custom) {
          for (let bundle of flycfg[ctag].custom) {
            if (fs.existsSync(path.resolve(ctx_path, bundle.src))) {
              bundle.cwd = app_dir;
              const compiler = new Compilers[ctag](ctx_path, bundle, web_dir_path);
              if (process.argv[3] == "watch") {
                compiler.watch();
              } else {
                await compiler.compile();
              }
            }
          }
        }

        if (ctag === "mjs" && flycfg.extra_mjs) {
          for (let emjs of flycfg.extra_mjs) {
            if (fs.existsSync(path.resolve(ctx_path, emjs.input))) {
              const compiler = new Compilers[ctag](ctx_path, {
                src: emjs.input,
                dst: emjs.output,
                cwd: app_dir
              }, web_dir_path);
              if (process.argv[3] == "watch") {
                compiler.watch();
              } else {
                await compiler.compile();
              }
            }
          }
        }
      }

      await FSUtil.read_dir(
        ctx_path,
        async (file_dir, file_name) => {
          // NOTE Don't need to do anything
          return true;
        },
        async (dir_path) => {
          const config_path = path.join(dir_path, ".webcontext.yaml");
          if (fs.existsSync(config_path)) {
            await _this.bundle_context(web_dir_path, dir_path, app_dir);
          }
          return true;
        }
      );
    } catch (e) {
      console.error(e.stack);
    }
  }
  

  static async run(config, zuncfg, basecfg) {
    try {
      const _this = new FlyPack(config, zuncfg);

      let app_dir = CWD;
      if (process.argv.length > 2) {
        for (let a = 3; a < process.argv.length; a++) {
          const app_dir_match = process.argv[a].match(/^--app-path=(.*)$/);
          if (app_dir_match) {
            app_dir = app_dir_match[1];
          }
        }
      }

      console.log("APP DIR", app_dir);

      if (basecfg) {
        if (Array.isArray(basecfg.services)) {
          for (let srv of basecfg.services) {
            if (srv.name == "flyweb") {
              config = ObjectUtil.force_add(config, srv.config)
            }
          }
        }
        
        const dir_routes_mg = config.webctx_mg;

        if (dir_routes_mg) {
          for (let web_dir of dir_routes_mg.web_dirs) {
            const web_dir_path = path.resolve(app_dir, web_dir.dir);
            if (fs.existsSync(web_dir_path)) {
              console.log("WEB DIRECTORY PATH:", web_dir_path);

              for (let ctag in Compilers) {
                await FSUtil.read_dir(web_dir_path, async (fpath, fname) => {
                  try {
                    if (
                      fname === dir_routes_mg.context_files.main
                    ) {
                      let selected = false;
                      if (config.selective) {
                        for (let webctx_dir of config.selective) {
                          const webctx_path = path.resolve(app_dir, webctx_dir);
                          if (fpath == webctx_path) {
                            selected = true;
                          }
                        }
                      }

                      if (!config.selective || selected) {

                        const flycfg_path = path.resolve(fpath, config.config_name);
                        let flycfg = undefined;
                        if (fs.existsSync(flycfg_path)) {
                          try {
                            flycfg = YAML.parse(fs.readFileSync(flycfg_path, 'utf8'));
                          } catch (e) {
                            console.error(e.stack);
                          }
                        }
                        if (!flycfg) flycfg = {};

                        if (!flycfg[ctag]) flycfg[ctag] = config[ctag];
                        if (!flycfg[ctag].src) flycfg[ctag].src = config[ctag].src;
                        if (!flycfg[ctag].dst) flycfg[ctag].dst = config[ctag].dst;
                        if (fs.existsSync(path.resolve(fpath, flycfg[ctag].src))) {
                          flycfg[ctag].cwd = app_dir;
                          const compiler = new Compilers[ctag](fpath, flycfg[ctag], web_dir_path);
                          if (process.argv[3] == "watch") {
                            compiler.watch();
                          } else {
                            await compiler.compile();
                          }
                        }

                        if (ctag === "mjs" && flycfg.extra_mjs) {
                          for (let emjs of flycfg.extra_mjs) {
                            if (fs.existsSync(path.resolve(fpath, emjs.input))) {
                              const compiler = new Compilers[ctag](fpath, {
                                src: emjs.input,
                                dst: emjs.output,
                                cwd: app_dir
                              }, web_dir_path);
                              if (process.argv[3] == "watch") {
                                compiler.watch();
                              } else {
                                await compiler.compile();
                              }
                            }
                          }
                        }

                      }


                    }
                  } catch (e) {
                    console.error(e.stack);
                  }
                });

              }



            }
          }
        }

        if (process.argv[3] !== "watch") process.exit();
      } else {
        await _this.bundle_context(CWD, CWD, app_dir);
      }

    } catch (e) {
      console.error(e.stack);
    }
  }

}
